# install npm

## products microservice
cd products
docker build -t product-service .
docker-compose up -d

## orders microservice
cd orders
docker build -t order-service .
docker-compose up -d

## API Gateway 

cd api-gateway
## change in URLs.js file 192.168.1.10 with your local IP
## using ifconfig to get it
docker build -t gateway-service .
$ docker-compose up -d


## Using API Gateway on browser


http://<your-local-IP>:3007/orders
http://<your-local-IP>:3007/products

